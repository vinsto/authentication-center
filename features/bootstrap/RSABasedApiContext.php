<?php

use Behat\Behat\Context\Context;
use phpseclib\Crypt\RSA;
use Vinsto\Crypt\RSA\RSAHandler;
use GuzzleHttp\Client;
use Behat\Testwork\ServiceContainer\Configuration\ConfigurationLoader;
use Behat\MinkExtension\Context\RawMinkContext;
use PHPUnit\Framework\Assert;
use Vinsto\Agency\Agent\RemoteServiceAgent;

/**
 * Defines application features from the specific context.
 */
class RSABasedApiContext extends RawMinkContext implements Context
{
    const VINSTO_WORLD_CONFIG_INFO_CENTER_YAML = __DIR__ . '/../../vendor/vinsto/vinsto-world/000_config/000_info_center.yaml';

    private $baseUrl;

    private static $keyStorage = [];

    /** @var array $infoCenterConfig */
    private $infoCenterConfig;

    /** @var string infoCenterURL */
    private $infoCenterURL;

    /** @var array $vinstoBaseConfig */
    private $vinstoBaseConfig;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        require __DIR__ . '/../../vendor/autoload.php';

        $config = (new ConfigurationLoader('BEHAT_PARAMS', getcwd() . '/behat.yml'))->loadConfiguration();
        $baseUrl = $config[0]['extensions']['Behat\\MinkExtension']['base_url'];
        $this->baseUrl = $baseUrl;

        $agent = new RemoteServiceAgent();
        list($this->infoCenterConfig, $this->infoCenterURL, $this->vinstoBaseConfig)
            = $agent->fetchInfoCenterAndBaseConfig(self::VINSTO_WORLD_CONFIG_INFO_CENTER_YAML);
    }

    /**
     *
     * @When I add a public key to authorized keys and save the keys in a temp folder
     */
    public function addKey()
    {
        $rsa = new RSA();
        $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);
        $key = $rsa->createKey();
        $privatekey= $this->processPrvkey($key['privatekey']);
        $publickey = $this->processPubkey($key['publickey']);

        #add key to authorized keys
        $authorizedKeysPath = __DIR__ . '/../../000_config/authorized-rsa_pub_keys';
        $content = file_get_contents($authorizedKeysPath);
        file_put_contents($authorizedKeysPath, trim($content) . "\n" . trim($publickey) . "\n");

        static::$keyStorage['prvkey'] = $privatekey;
        static::$keyStorage['pubkey'] = $publickey;
    }

    /**
     * @When I generate a signature of a random message and prepare data to be sent to API
     */
    public function signature()
    {
        $rsaHandler = new RSAHandler();
        $rsaHandler->setSignatureMode(RSA::SIGNATURE_PKCS1);
        $rsaHandler->setPrivateKey(static::$keyStorage['prvkey']);
        $rsaHandler->setPublicKey(static::$keyStorage['pubkey']);


        $msg = base64_encode(bin2hex(random_bytes(8)));
        $signature = $rsaHandler->sign($msg, $rsaHandler->getPrivateKey()/*, $rsaHandler->getSignatureMode()*/);

        $rsa_auth_header = [
            'pubkey' => $rsaHandler->getPublicKey(),
            'message' => $msg,
            'signature' => $signature,
            'myemail' => 'adminadmin@exampleexample.com', #### EMAIL CHANGE_UPON_DEPLOYMENT
        ];
        #rsaHeader  ---- to be saved
        $rsa_auth_header = json_encode($rsa_auth_header);
        static::$keyStorage['rsa_auth_header'] = $rsa_auth_header;

        $data = [
            'email' => 'adminadmin@exampleexample.com',  #### EMAIL CHANGE_UPON_DEPLOYMENT
        ];
        #requestBody_json  ---- to be saved
        $json = json_encode($data);
        static::$keyStorage['body_json'] = $json;
    }

    /**
     * @Given /^I send a post request with the data to uri "([^"]*)" and I get cerrect response$/
     */
    public function request($uri)
    {
        $json = static::$keyStorage['body_json'];
        $rsa_auth_header = static::$keyStorage['rsa_auth_header'];

        $client = new Client();
        $authCenterUrl = $this->getAuthCenterUrl();
        $response = $client->post($authCenterUrl . $uri, [
            'headers' => [
                'X-RSA-CONTENT' => $rsa_auth_header,
            ],
            'form_params' => [
                'json' => $json,
            ]
        ]);

        $userPasswordAtAuthenticationCenter = (string) $response->getBody();

        Assert::assertEquals(200, $response->getStatusCode());
        Assert::assertGreaterThan(10, strlen($userPasswordAtAuthenticationCenter));

    }

    /**
     * @Given /^I send a post request without right headers to uri "([^"]*)" and I get errors$/
     */
    public function requestWithoutHeader($uri)
    {
        $json = static::$keyStorage['body_json'];;

        $url = $this->baseUrl . $uri;
        $res = `curl -X POST -i -d json='$json' $url`;

        $matches = [];
        preg_match("/^http\S+\s+(\d{3})/i", $res, $matches);

        Assert::assertGreaterThan(399, intval($matches[1]));
    }

    protected function processPrvkey(string $key): string
    {
        $key = preg_replace("/-{3,}BEGIN RSA PRIVATE KEY-{3,}/", '', $key);
        $key = preg_replace("/-{3,}END RSA PRIVATE KEY-{3,}/", '', $key);
        $key = preg_replace("/\r\n/", '', $key);

        return $key;
    }

    protected function processPubkey(string $key): string
    {
        $key = preg_replace("/-{3,}BEGIN PUBLIC KEY-{3,}/", '', $key);
        $key = preg_replace("/-{3,}END PUBLIC KEY-{3,}/", '', $key);
        $key = preg_replace("/\r\n/", '', $key);

        return $key;
    }

    protected function getAuthCenterUrl(): string
    {
        $protocal = $this->vinstoBaseConfig['global']['AUTHENTICATION_CENTER_df20c3b__Protocal'];
        $host = $this->vinstoBaseConfig['global']['AUTHENTICATION_CENTER_df20c3b__Host'];
        $port = $this->vinstoBaseConfig['global']['AUTHENTICATION_CENTER_df20c3b__Port'];

        return "$protocal://$host:$port";
    }
}
