@operational-unit-testing @functional-testing
Feature: Authentication
  As a security conscious developer I wish to ensure that only valid users can access our website.

  Scenario: Login in successfully to my website
    When I am on "/login"
    And I fill in "email" with "adminadmin@exampleexample.com"
    And I fill in "password" with "adminadmin@exampleexample.com"
    And I press "Sign in"
    Then I should see "Admin Dashboard"
