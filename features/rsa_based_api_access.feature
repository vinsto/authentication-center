@operational-unit-testing  @functional-testing
Feature: RSA based API access check
  As a security conscious developer I wish to ensure that only clients using valid RSA public keys can access some APIs

  Scenario: Getting password of some user (e.g., for/within OAuth2)
    When I add a public key to authorized keys and save the keys in a temp folder
    And I generate a signature of a random message and prepare data to be sent to API
    Then I send a post request with the data to uri "/api/get-user-password" and I get cerrect response
    Then I send a post request without right headers to uri "/api/get-user-password" and I get errors
