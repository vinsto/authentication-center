<?php

namespace App\Controller;

use App\Service\JWTService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Yaml\Yaml;

class AdminSecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request, JWTService $JWTService): Response
    {
         if ($this->getUser()) {

             if ( (!empty($request->get('need_jwt'))) &&
                 (!empty($url0 = $request->get('redirect_url')))
             ) {
                 $securityConfig = Yaml::parse(file_get_contents(__DIR__ . '/../../vendor/vinsto/vinsto-world/000_config/000_security.yaml'));
                 $tokenLabel = $securityConfig['jwt']['token-label'];

                 $jwtToken = $JWTService->generateToken($this->getUser());
                 $url = $url0 . "?$tokenLabel=$jwtToken";

                 return $this->redirect($url);
             }

             return $this->redirectToRoute('app_admin_homepage');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $data =  [
            'last_username' => $lastUsername,
            'error' => $error,
//            'redirectUrl' => $request->query->get('redirect_url'),
//            'needJWT' => 1
        ];

        if (!empty($redirect_url = $request->query->get('redirect_url'))) {
            $data['redirectUrl'] = $redirect_url;
        }

        if (!empty($request->query->get('need_jwt'))) {
            $data['needJWT'] = 1;
        }

        return $this->render('security/login.html.twig', $data);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
