<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Form\AdminType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdminRegistrationController extends AbstractController
{
    const VINSTO_WORLD_CONFIG_INFO_CENTER_YAML = __DIR__ . '/../../vendor/vinsto/vinsto-world/000_config/000_info_center.yaml';

    /** @var array $infoCenterConfig */
    private $infoCenterConfig;

    /** @var string infoCenterURL */
    private $infoCenterURL;


    public function __construct()
    {
        $this->infoCenterConfig = Yaml::parse(file_get_contents(self::VINSTO_WORLD_CONFIG_INFO_CENTER_YAML));
        $this->infoCenterURL =
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Protocal'] . '://' .
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Host'] . ':' .
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Port'];
    }


    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/register", name="admin_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $admin = new Admin();
        $form = $this->createForm(AdminType::class, $admin);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($admin, $admin->getPassword());
            $admin->setPassword($password);

            $admin->setRoles(['ROLE_ADMIN']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($admin);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('app_admin_homepage');
        }

        return $this->render(
            'admin/register.html.twig',
            array('form' => $form->createView())
        );
    }
}
