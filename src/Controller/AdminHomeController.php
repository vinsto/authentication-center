<?php

namespace App\Controller;

use GuzzleHttp\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Vinsto\Agency\Agent\RemoteServiceAgent;

class AdminHomeController extends AbstractController
{
    /**
     * Todo: [< TestCase >]
     *   do:
     *     $agent = new RemoteServiceAgent(;
     *     $url4ParsingRemoteService = $agent->getInfoCenterUrl4ParsingRemoteService();
     *     $layoutServiceApiUrl = $agent->parse('vrs--view-layout--endpoint');
     *   expetation:
     *     - get correct api urls of and from info-center
     */

    const VINSTO_WORLD_CONFIG_INFO_CENTER_YAML = __DIR__ . '/../../vendor/vinsto/vinsto-world/000_config/000_info_center.yaml';

    /** @var array $infoCenterConfig */
    private $infoCenterConfig;

    /** @var string infoCenterURL */
    private $infoCenterURL;


    public function __construct()
    {
        $this->infoCenterConfig = Yaml::parse(file_get_contents(self::VINSTO_WORLD_CONFIG_INFO_CENTER_YAML));
        $this->infoCenterURL =
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Protocal'] . '://' .
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Host'] . ':' .
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Port'];
    }


    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/dashboard", name="app_admin_homepage")
     */
    public function index(): Response
    {
        return $this->render('admin/home.html.twig');
    }

    /**
     * @Route("/admin/token-login")
     */
    public function tokenLogin(Request $request,  KernelInterface $kernel): Response
    {
        $token = $request->query->get('access-token');

        $rs = new RemoteServiceAgent();
        if ($rs->checkAccessToken($token)) {
            setcookie('access-token', $token, time() + 3600, '/');

//            return new JsonResponse('OK');
//            header("Location: http://localhost:4739/admin/token-login?access-token=$token");
//            die;
            return $this->redirect("$this->infoCenterURL/admin/token-login?next=product-cellphone&access-token=$token");

        }else {

            throw new \Exception('Something wrong, maybe access token not valid!', 1615316778);
        }
    }
}
