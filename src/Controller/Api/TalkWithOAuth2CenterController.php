<?php

namespace App\Controller\Api;

use App\Entity\Admin;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;

class TalkWithOAuth2CenterController extends AbstractController
{
    /**
     * @Route("/api/get-user-password", defaults={"need_rsa"=true})
     */
    public function test(RequestStack $requestStack, EntityManagerInterface $em)
    {
        $request = $requestStack->getCurrentRequest();
        $data = json_decode($request->request->get('json'),true);

        $admin = $em->getRepository(Admin::class)->findOneByEmail($data['email']);
        $x = 1;

        return new Response($admin->getPassword());
    }

}
