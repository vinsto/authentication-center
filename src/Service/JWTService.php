<?php

namespace App\Service;

use App\Entity\Admin;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Yaml\Yaml;
use Vinsto\Agency\Agent\RemoteServiceAgent;

class JWTService {

    const VINSTO_WORLD_CONFIG_INFO_CENTER_YAML = __DIR__ . '/../../vendor/vinsto/vinsto-world/000_config/000_info_center.yaml';

    /** @var array $infoCenterConfig */
    private $infoCenterConfig;

    /** @var array $baseConfig */
    private $baseConfig;

    /** @var string infoCenterURL */
    private $infoCenterURL;

    public function __construct(RequestStack $requestStack=null)
    {
        $agent = new RemoteServiceAgent();
        list($this->infoCenterConfig, $this->infoCenterURL, $this->baseConfig)
            = $agent->fetchInfoCenterAndBaseConfig(self::VINSTO_WORLD_CONFIG_INFO_CENTER_YAML);
    }

    public function generateToken(UserInterface $user)
    {
        $datetime = new \Datetime('+1 hour');

        $header = [
            'alg' => 'hash_hmac_sha256',
        ];

        if ($user instanceof Admin) {
            $userEntity = Admin::class;
        }

        $content = [
            'valid_until' => $datetime->getTimestamp(),
            'user_entity' => $userEntity,
            'user_id' => $user->getId(),
        ];

        $head = base64_encode(json_encode($header));
        $body = base64_encode(json_encode($content));
        $message = $head . '.' . $body;

        $secret = $this->baseConfig['global']['AUTHENTICATION_CENTER_df20c3b__System_Secret'];
        $signiture = hash_hmac("sha256", $message, $secret);

        $code  = $message . '.' . $signiture;

        return $code;
    }
}
