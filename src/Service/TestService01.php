<?php

namespace App\Service;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Vinsto\Agency\Agent\RemoteServiceAgent;

class TestService01 {

    public function testFunc($str)
    {
        return "you've input '$str'";
    }
}
