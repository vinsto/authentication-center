<?php

namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use phpseclib\Crypt\RSA;
use Vinsto\Crypt\RSA\RSAHandler;

class RSAAuthenticator extends AbstractGuardAuthenticator
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        return $request->attributes->get('need_rsa');
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        return $request->headers->get('X-RSA-CONTENT');
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (null === $credentials) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        // The "username" in this case is the apiToken, see the key `property`
        // of `your_db_provider` in `security.yaml`.
        // If this returns a user, checkCredentials() is called next:

        $rsaContent = json_decode($credentials, true);
        $publickey = trim($rsaContent['pubkey']);
        $isExistInAuthorizedPubKeys = in_array($publickey, $this->getAuthorizedPubKeys());
        if (!$isExistInAuthorizedPubKeys) {
            return null;
        }
        $isVerifiedPubkey = $this->verifyPubkey($publickey, $rsaContent['message'], $rsaContent['signature']);
        if (!$isVerifiedPubkey) {
            return null;
        }

        return $userProvider->loadUserByUsername($rsaContent['myemail']);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // Check credentials - e.g. make sure the password is valid.
        // In case of an API token, no credential check is needed.

        // Return `true` to cause authentication success
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }

    protected function getAuthorizedPubKeys(): array
    {
        $filetext = file_get_contents(__DIR__ . '/../../000_config/authorized-rsa_pub_keys');
        return explode("\n", $filetext);
    }

    protected function verifyPubkey(string $publickey, string $msg, string $signature): bool
    {
        /**
         * Todo: 使用过的$msg无效
         */
        $rsaHandler = new RSAHandler();
        $res = $rsaHandler->verify($msg, $signature, $publickey);

        return $res;
    }
}
