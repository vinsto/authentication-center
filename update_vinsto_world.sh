#!/usr/bin/env bash

rm -rf vendor/vinsto
composer clear-cache
composer update vinsto/vinsto-world
composer update vinsto/vinsto-core
