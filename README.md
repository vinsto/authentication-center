# Symfony Web Base 7.1

* PHP 7.1
* Symfony 4
* Doctrine ORM

### Getting up and running
- Install composer dependencies `composer install`
- Create schema `bin/console doctrine:schema:create`
- Load fixtures `bin/console doctrine:fixtures:load`
